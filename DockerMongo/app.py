import os
import flask
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY


client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
#reset the tododb database
client.drop_database('tododb')
db = client['tododb']


@app.route('/')
def todo():
    return render_template('todo.html')

@app.route('/new')
def new():
    control_dist = request.args.get('km', 1e5, type=float)
    open_time = request.args.get('open_time')
    close_time = request.args.get('close_time')
    dist = request.args.get('distance', 200, type=float)
    start_time = request.args.get('start_time')

    item_doc = {
        'brevet_dist': dist,
        'start_time': start_time,
        'control_dist': control_dist,
        'open': open_time,
        'close': close_time
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))


@app.route('/display', methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('display.html', items=items)


@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 1e5, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    dist = request.args.get('distance', 200, type=float)
    start_time = request.args.get('start_time')

    print("km ", km)
    print("dist ", dist)
    print("start_time ", start_time)
    brevet_start_time = arrow.get(start_time)
    brevet_start_time = brevet_start_time.shift(hours=+8).isoformat()

    open_time = acp_times.open_time(km, dist, brevet_start_time)
    close_time = acp_times.close_time(km, dist, brevet_start_time)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
