Author - Noah Kruss

Contact info - nkruss@uoregon.edu

# Project Description:

A reimplement the RUSA ACP control time calculator using flask and ajax and MongoDB.

When run the project runs a web application is started using flask where the user can input specified brevet distances, start time and control distances into a table. The web application takes these inputs and updates the web page dynamically using ajax adding in calculated control open and close times.

In parallel when the web application is started up a empty database named tododb is created to store brevet information,

The open and close functions are based off of the ACP brevet control time rules given here (https://rusa.org/pages/acp-brevet-control-times-calculator) using the french rules. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.

Upon clicking the "Submit" button the inputed control distance information will be stored in a database and the webpage will be reloaded. The Upon clicking the "Display" button the user will be re-directed to a new webpage where the submitted control distances and their related information will be displayed.


## Brevet Calculation notes

If the user inputs a invalid control distance such as the control distance being negative or the control distance begin more then 20% larger then the brevet distance, then the appropriate error message will be displayed in the open and close columns

If the user inputs a control distance that is not a number, then the open and close time columns will remain blank until the control distance is updated to a number value.

The implementation for open and close times will return times based off PST (west coast time) and do not take into account daylight savings

## Database functionality notes

If the user clicks the "Submit" button with no control distances entered then the webpage will be reloaded but nothing will be uploaded into the database.

If the user clicks the "Submit" button with invalid control distances inputted then the brevet data will be uploaded to the database with the error messages which will be displayed in the display page 

If the user clicks the "Display" button with no controls stored in the database they will still be taken to the display page but there will be a hyperlink to return them back to the calculator.
